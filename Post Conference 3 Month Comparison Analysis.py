#!/usr/bin/env python
# coding: utf-8
# %% [markdown]
#
# # # Conference 3 Month Analysis  
# # ### QGI: Brian Preisler     
# # ### April 2019  
# # 
#
# # 1. Read in conference attendees that have been matched with Acts ID (in seperate Python script)   
# # ***
# # 2. Run through involvement queries  
# # ***
# # 3. Place all data into master table  
# # ***
# # 4. Compare to Non Conf Attendees  
# #     -Compare against historical table?
#
# #SHOULD CONSIDER CHANGING TO A .ENV file system


# %%
#Import needed libraries
import time
import datetime
import pymysql
import xlsxwriter
import smtplib
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import csv
import numpy as np
from datetime import datetime, timedelta
import dateutil.parser
import pymysql.cursors
import sshtunnel


# %%


#Set custom parameters
conf_start = '2019-01-01'
spring_end = '2019-04-30'
fall_start = '2018-09-01'
conf_students_at_FOCUS_campuses = 8216
data = pd.read_csv('C:\\Users\\brian.preisler\\Dropbox\\Growth\\Data Analysis\\Event Analysis\\Data Cleaning\\SEEK19_ids_matched.csv', encoding='latin1') 


# %%


###
#Simple query test to see if connection can be established:
###

#use task manager to end all 'mysql' tasks or tunnel will not establish
# path to SSH private key in the same folder as the jupyter notebook
ssh_key_path=r'id_rsa'

# only proceed if file exists
if os.path.isfile(ssh_key_path):
    
    # create tunnel
    with sshtunnel.SSHTunnelForwarder(
            ('acts247.focus.org', 22),
            ssh_username='bpadmin',
            ssh_pkey=ssh_key_path, 
            remote_bind_address=('127.0.0.1',3306),
            local_bind_address=('127.0.0.1',3306)
    ) as tunnel:

        # Connect to the database
        connection = pymysql.connect(host='localhost',
                                     user='brian',
                                     password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                     db='acts247',
                                     cursorclass=pymysql.cursors.DictCursor)

        try:
            with connection.cursor() as cursor:
                # Read a single record
                sql = "SELECT `id`, `email` FROM `users` WHERE `email`=%s"
                cursor.execute(sql, ('brian.preisler@focus.org',))
                result = cursor.fetchone()
                print(result)
        finally:
            print('success')
            connection.close()
else:
    print('Could not find ssh key at ' + ssh_key_path)


# %%


#Data cleaning
#Check total IDs and compare to those that have been matched with Acts IDs

acts_attendees = data
acts_attendees['best_id'] = acts_attendees['best_id'].astype('object')
len_acts_attendees = len(acts_attendees)
print('Student Conf Attendees Who Have an Acts 2:47 ID: ',len_acts_attendees)


# %%


#Place ids in list to use as parameter in following query
ids = list(acts_attendees['best_id'])


# ## Conference Before/After Queries

# ### Disciples at Start of Conf Query:

# %%


### Disciples at start of conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, BS.leader_id, min(BSA.date) \n
                from users as U \n
                left join bible_studies as BS on BS.leader_id = U.id \n
                left join bible_study_attendances as BSA on BSA.bible_study_id = BS.id \n
                join disciples as D on D.user_id = U.id \n
                where U.id IN %s \n
                group by U.id \n
                having earliest_discipleship_start_date(U.id) <= %s \n
                and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s) \n
                """
            args = [ids, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            D_at_start_conf = len(result)
            print('Disciples at start of conf: ' , D_at_start_conf)
    finally:
        connection.close()


# %%


### BSPx4 conversion to D

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and (earliest_discipleship_start_date(U.id) >= %s or earliest_discipleship_start_date(U.id) IS NULL)
                        and BSA.date between %s and %s
                        and U.id IN %s \n
                        group by U.id
                        having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start,fall_start, conf_start,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            BSPx4_not_D_at_start_conf = len(result)
            print('BSPx4 but not Disciples at start of conf: ' , BSPx4_not_D_at_start_conf)
    finally:
        connection.close()


# %%


### BSPx4 conversion to D

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_discipleship_start_date(U.id) between %s and %s
                        and BSA.date between %s and %s
                        and U.id IN %s \n
                        group by U.id \n
                        having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start, spring_end, fall_start, conf_start,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            BSPx4_became_D_3_post = len(result)
            print('BSPx4 that became D in 3 months post conf: ' , BSPx4_became_D_3_post)
    finally:
        connection.close()


# %%


### Disciples that hadn't led a BS at start of conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, BS.leader_id, min(BSA.date) \n
                from users as U \n
                left join bible_studies as BS on BS.leader_id = U.id \n
                left join bible_study_attendances as BSA on BSA.bible_study_id = BS.id \n
                join disciples as D on D.user_id = U.id \n
                where U.id IN %s \n
                #and D.level != 1
                group by U.id \n
                having earliest_discipleship_start_date(U.id) <= %s \n
                and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s) \n
                and (min(BSA.date) >= %s OR min(BSA.date) IS NULL)"""
            args = [ids, conf_start, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            D_not_SLBS_start_conf = len(result)
            print('Disciples that hadn\'t led a BS at start of conf: ' , D_not_SLBS_start_conf)
    finally:
        connection.close()


# %%


### Disciples that led a BS in 3 months post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, BS.leader_id, min(BSA.date) \n
                from users as U \n
                left join bible_studies as BS on BS.leader_id = U.id \n
                left join bible_study_attendances as BSA on BSA.bible_study_id = BS.id \n
                join disciples as D on D.user_id = U.id \n
                where U.id IN %s \n
                #and D.level != 1
                group by U.id \n
                having earliest_discipleship_start_date(U.id) <= %s \n
                and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s) \n
                and min(BSA.date) between %s and %s"""
            args = [ids, conf_start, conf_start, conf_start, spring_end]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            D_became_SLBS_3_post = len(result)
            print('Disciples that led a BS in 3 months post conf: ' ,D_became_SLBS_3_post)
    finally:
        connection.close()


# ### Disciples Who Weren't a DM Before Conf
# Query and result

# %%


### Disciples who weren't DM at start of conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,min(D.start_date),DM.parent_user_id,min(DM.start_date)
                        from users as U
                        join disciples as D on D.user_id = U.id
                        left join disciples as DM on DM.parent_user_id = U.id
                        where
                        U.id IN %s
                        #and D.level != 1
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and (min(DM.start_date) >= %s OR min(DM.start_date) IS NULL)
                        """
            args = [ids, conf_start, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            D_not_DM_at_start_conf = len(result)
            print('Disciples who weren\'t DM at start of conf',D_not_DM_at_start_conf)
    finally:
        connection.close()


# ## Disciples Who Became DM in 3 Month Post
# Query and result

# %%


### Disciples who became DM in 3 months post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,min(D.start_date),DM.parent_user_id,min(DM.start_date)
                        from users as U
                        join disciples as D on D.user_id = U.id
                        left join disciples as DM on DM.parent_user_id = U.id
                        where
                        U.id IN %s
                        #and D.level != 1
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and min(DM.start_date) between %s and %s
                        """
            args = [ids, conf_start,conf_start, conf_start, spring_end]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            D_became_DM_3_post = len(result)
            print('Disciples who became DM in 3 months post conf: ' ,D_became_DM_3_post)
    finally:
        connection.close()


# %%


### first BS attendance post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_bs_attendance_date(U.id) between %s and %s
                        and U.id IN %s \n
                        group by U.id \n
                        #having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start, spring_end,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            first_BS_att_post_conf = len(result)
            print('Students that attended their first BS after Conf: ' , first_BS_att_post_conf)
    finally:
        connection.close()


# %%


### first BS attendance led to BSPx4 post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_bs_attendance_date(U.id) between %s and %s
                        and U.id IN %s \n
                        group by U.id \n
                        having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start, spring_end,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            first_BS_att_post_conf_then_BSPx4 = len(result)
            print('Students that attended their first BS after Conf and Became BSPx4: ' , first_BS_att_post_conf_then_BSPx4)
    finally:
        connection.close()


# %%


### students who had attended first BS before conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_bs_attendance_date(U.id) <= %s
                        and U.id IN %s \n
                        group by U.id \n
                """
            args = [conf_start, ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            att_first_BS_before_conf = len(result)
            print('Students that attended their first BS before Conf: ' , att_first_BS_before_conf)
    finally:
        connection.close()


# %%


conf_D_became_SLBS = round(D_became_SLBS_3_post/D_not_SLBS_start_conf,2)
print(100*conf_D_became_SLBS)


# %%


conf_df2 = pd.DataFrame(
    {"Type": ['Students with Acts 2:47 ID', "Disciples at Start of Conf","BSPx4 who became D 3 months after Conf", 
             "BSPx4 not D at Start of Conf",
            "% BSPx4 Who Became D","D who became SLBS 3 months after Conf", 
            "D who weren't SLBS at start of Conf", "% D Who Became SLBS",
            "D who became DM 3 months after Conf","D who weren't DM at Start of Conf", 
            "% D Who Became DM",'Students that attended their first BS after Conf and Became BSPx4',
             'Students that attended their first BS after Conf', '% New BSPx1 Who Became BSPx4',
             'Attended first BS before conf', 'Total Students at Conf from FOCUS Campsues',
             'Students Who Could Have Attended First BS After Conf','Att first BS post conf',
              '% Who Attended First BS Post Conf'],        
        "All Conf Students":[len_acts_attendees, D_at_start_conf,BSPx4_became_D_3_post, BSPx4_not_D_at_start_conf,  
                     round(100*(BSPx4_became_D_3_post/BSPx4_not_D_at_start_conf)),
                    D_became_SLBS_3_post,D_not_SLBS_start_conf, 
                    round(100*(D_became_SLBS_3_post/D_not_SLBS_start_conf)),
                    D_became_DM_3_post,D_not_DM_at_start_conf, 
                    round(100*(D_became_DM_3_post/D_not_DM_at_start_conf)),
                    first_BS_att_post_conf_then_BSPx4,first_BS_att_post_conf,
                     round(100*(first_BS_att_post_conf_then_BSPx4/first_BS_att_post_conf)),
                     att_first_BS_before_conf,conf_students_at_FOCUS_campuses,
                     conf_students_at_FOCUS_campuses-att_first_BS_before_conf,
                             first_BS_att_post_conf,
                     round(100*(first_BS_att_post_conf/(conf_students_at_FOCUS_campuses-att_first_BS_before_conf)))
                    ]},)

conf_df2


# %%


### Nonattendee Disciples at start of conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, BS.leader_id, min(BSA.date) \n
                from users as U \n
                left join bible_studies as BS on BS.leader_id = U.id \n
                left join bible_study_attendances as BSA on BSA.bible_study_id = BS.id \n
                join disciples as D on D.user_id = U.id \n
                where U.id NOT IN %s \n
                group by U.id \n
                having earliest_discipleship_start_date(U.id) <= %s \n
                and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s) \n
                """
            args = [ids, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_D_at_start_conf = len(result)
            print('Nonattendee Disciples at start of conf: ' , nonattendee_D_at_start_conf)
    finally:
        connection.close()


# %%


### Nonattendee BSPx4 conversion to D

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and (earliest_discipleship_start_date(U.id) >= %s or earliest_discipleship_start_date(U.id) IS NULL)
                        and BSA.date between %s and %s
                        and U.id NOT IN %s \n
                        group by U.id
                        having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start,fall_start, conf_start,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_BSPx4_not_D_at_start_conf = len(result)
            print('Nonattendee BSPx4 but not Disciples at start of conf: ' , nonattendee_BSPx4_not_D_at_start_conf)
    finally:
        connection.close()


# %%


###Nonconf BSPx4 conversion to D

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_discipleship_start_date(U.id) between %s and %s
                        and BSA.date between %s and %s
                        and U.id NOT IN %s \n
                        group by U.id \n
                        having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start, spring_end, fall_start, conf_start,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_BSPx4_became_D_3_post = len(result)
            print('Nonconf BSPx4 that became D in 3 months post conf: ' , nonattendee_BSPx4_became_D_3_post)
    finally:
        connection.close()


# %%


### Nonconf Disciples that hadn't led a BS at start of conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, BS.leader_id, min(BSA.date) \n
                from users as U \n
                left join bible_studies as BS on BS.leader_id = U.id \n
                left join bible_study_attendances as BSA on BSA.bible_study_id = BS.id \n
                join disciples as D on D.user_id = U.id \n
                where U.id NOT IN %s \n
                group by U.id \n
                having earliest_discipleship_start_date(U.id) <= %s \n
                and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s) \n
                and (min(BSA.date) >= %s OR min(BSA.date) IS NULL)"""
            args = [ids, conf_start, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_D_not_SLBS_start_conf = len(result)
            print('Nonconf Disciples that hadn\'t led a BS at start of conf: ' , nonattendee_D_not_SLBS_start_conf)
    finally:
        connection.close()


# %%


### Nonconf Disciples that led a BS in 3 months post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, BS.leader_id, min(BSA.date) \n
                from users as U \n
                left join bible_studies as BS on BS.leader_id = U.id \n
                left join bible_study_attendances as BSA on BSA.bible_study_id = BS.id \n
                join disciples as D on D.user_id = U.id \n
                where U.id NOT IN %s \n
                group by U.id \n
                having earliest_discipleship_start_date(U.id) <= %s \n
                and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s) \n
                and min(BSA.date) between %s and %s"""
            args = [ids, conf_start, conf_start, conf_start, spring_end]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_D_became_SLBS_3_post = len(result)
            print('Nonconf Disciples that led a BS in 3 months post conf: ' ,nonattendee_D_became_SLBS_3_post)
    finally:
        connection.close()


# %%


### Nonconf Disciples who weren't DM at start of conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,min(D.start_date),DM.parent_user_id,min(DM.start_date)
                        from users as U
                        join disciples as D on D.user_id = U.id
                        left join disciples as DM on DM.parent_user_id = U.id
                        where
                        U.id NOT IN %s
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and (min(DM.start_date) >= %s OR min(DM.start_date) IS NULL)
                        """
            args = [ids, conf_start,conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_D_not_DM_at_start_conf = len(result)
            print('Nonconf Disciples who weren\'t DM at start of conf',nonattendee_D_not_DM_at_start_conf)
    finally:
        connection.close()


# %%


### Nonconf Disciples who became DM in 3 months post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,min(D.start_date),DM.parent_user_id,min(DM.start_date)
                        from users as U
                        join disciples as D on D.user_id = U.id
                        left join disciples as DM on DM.parent_user_id = U.id
                        where
                        U.id NOT IN %s
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and min(DM.start_date) between %s and %s
                        """
            args = [ids, conf_start,conf_start, conf_start, spring_end]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_D_became_DM_3_post = len(result)
            print('Nonconf Disciples who became DM in 3 months post conf: ' ,nonattendee_D_became_DM_3_post)
    finally:
        connection.close()


# %%


### nonconf first BS attendance post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_bs_attendance_date(U.id) between %s and %s
                        and U.user_role_type_id = 3
                        and U.id NOT IN %s \n
                        group by U.id \n
                        #having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start, spring_end,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_first_BS_att_post_conf = len(result)
            print('Nonconf Students that attended their first BS after Conf: ' , nonattendee_first_BS_att_post_conf)
    finally:
        connection.close()


# %%


### Nonconf first BS attendance led to BSPx4 post conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_bs_attendance_date(U.id) between %s and %s
                        and U.user_role_type_id = 3
                        and U.id NOT IN %s \n
                        group by U.id \n
                        having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4
                """
            args = [conf_start, spring_end,ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_first_BS_att_post_conf_then_BSPx4 = len(result)
            print('Nonconf Students that attended their first BS after Conf and Became BSPx4: ' , nonattendee_first_BS_att_post_conf_then_BSPx4)
    finally:
        connection.close()


# %%


### nonconf students who had attended first BS before conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and earliest_bs_attendance_date(U.id) <= %s
                        and U.id NOT IN %s \n
                        group by U.id \n
                """
            args = [conf_start, ids]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonattendee_att_first_BS_before_conf = len(result)
            print('Students that attended their first BS before Conf: ' , nonattendee_att_first_BS_before_conf)
    finally:
        connection.close()


# %%


non_conf_df = pd.DataFrame(
    {"Type": ['Students with Acts 2:47 ID', "Disciples at Start of Conf","BSPx4 who became D 3 months after Conf", 
             "BSPx4 not D at Start of Conf",
            "% BSPx4 Who Became D","D who became SLBS 3 months after Conf", 
            "D who weren't SLBS at start of Conf", "% D Who Became SLBS",
            "D who became DM 3 months after Conf","D who weren't DM at Start of Conf", 
            "% D Who Became DM",'Students that attended their first BS after Conf and Became BSPx4',
             'Students that attended their first BS after Conf', '% New BSPx1 Who Became BSPx4',
             'Attended first BS before conf', 'Total Students at Conf from FOCUS Campsues',
             'Students Who Could Have Attended First BS After Conf', 'Att first BS post conf',
              '% Who Attended First BS Post Conf'],        
        "Nonconf Students":[len_acts_attendees, nonattendee_D_at_start_conf,nonattendee_BSPx4_became_D_3_post, nonattendee_BSPx4_not_D_at_start_conf,  
                     round(100*(nonattendee_BSPx4_became_D_3_post/nonattendee_BSPx4_not_D_at_start_conf)),
                    nonattendee_D_became_SLBS_3_post,nonattendee_D_not_SLBS_start_conf, 
                    round(100*(nonattendee_D_became_SLBS_3_post/nonattendee_D_not_SLBS_start_conf)),
                    nonattendee_D_became_DM_3_post,nonattendee_D_not_DM_at_start_conf, 
                    round(100*(nonattendee_D_became_DM_3_post/nonattendee_D_not_DM_at_start_conf)),
                    nonattendee_first_BS_att_post_conf_then_BSPx4,nonattendee_first_BS_att_post_conf,
                     round(100*(nonattendee_first_BS_att_post_conf_then_BSPx4/nonattendee_first_BS_att_post_conf)),
                     nonattendee_att_first_BS_before_conf,conf_students_at_FOCUS_campuses,
                     conf_students_at_FOCUS_campuses-att_first_BS_before_conf,nonattendee_first_BS_att_post_conf,
                     round(100*(nonattendee_first_BS_att_post_conf/(conf_students_at_FOCUS_campuses-att_first_BS_before_conf)))
                    ]},)

non_conf_df


# %%


master_df = pd.merge(conf_df2, non_conf_df, left_on = 'Type', right_on = 'Type', how = 'left')

master_df


# ## MLT Question Deep Dive
# Look at campus level results to compare fruitfulness

# %%


### MLT requests:
# Get campus level results to compare fruitfulness

#D DMs at conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,C.name
                        from users as U
                        join disciples as D on D.user_id = U.id
                        left join disciples as DM on DM.parent_user_id = U.id
                        left join campuses as C on C.id = U.campus_id
                        where
                        U.id IN %s
                        and C.region_id NOT IN (10,26,44,45)
                        #and D.level != 1
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and min(DM.start_date) between %s and %s
                """
            args = [ids, conf_start, conf_start, conf_start, spring_end]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            conf_dship_length = result
            conf_dship_length_df = pd.read_sql(sql,connection,params= args)
            #print(conf_dship_length_df)
    finally:
        connection.close()
        
d_dm_table = pd.pivot_table(conf_dship_length_df, index=['name'], aggfunc = np.count_nonzero)

d_dm_table.to_csv('C:\\Users\\brian.preisler\\Dropbox\\Growth\\Data Analysis\\Event Analysis\\SEEK 2019\\SEEK19 Impact\\d_dm.csv')


# %%


### MLT requests:
# Get campus level results to compare fruitfulness

#D not  DMs at conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id, C.name
                        from users as U
                        join disciples as D on D.user_id = U.id
                        left join disciples as DM on DM.parent_user_id = U.id
                        left join campuses as C on C.id  = U.campus_id
                        where
                        U.id IN %s
                        and C.region_id NOT IN(10,26,44,45)
                        #and D.level != 1
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and (min(DM.start_date) >= %s OR min(DM.start_date) IS NULL)
                """
            args = [ids, conf_start, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            conf_dship_length = result
            conf_dship_length_df = pd.read_sql(sql,connection,params= args)
            #print(conf_dship_length_df)
    finally:
        connection.close()
        
d_not_dm_table = pd.pivot_table(conf_dship_length_df, index=['name'], aggfunc = np.count_nonzero)

d_not_dm_table.to_csv('C:\\Users\\brian.preisler\\Dropbox\\Growth\\Data Analysis\\Event Analysis\\SEEK 2019\\SEEK19 Impact\\d_not_dm.csv')


# %%


### length in dship for conf attendee disciples

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """Select U.id, earliest_discipleship_start_date(U.id) as 'earliest', 
                        case when latest_discipleship_end_date(U.id) IS NULL then curdate() else latest_discipleship_end_date(U.id) END as 'latest' 
                        
                        from users as U 
                        join disciples as D on D.user_id = U.id
                        join campuses as C on C.id = U.campus_id
                        where U.user_role_type_id = 3 
                        and U.school_year_id IN (1,2,3,4,5)
                        and U.id IN %s
                        and C.region_id NOT IN (10,26,44,45)
                        and (latest_discipleship_end_date(U.id) >= %s OR latest_discipleship_end_date(U.id) IS NULL) 
                        and earliest_discipleship_start_date(U.id) <= %s
                        group by U.id
                """
            args = [ids, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            conf_dship_length = result
            conf_dship_length_df = pd.read_sql(sql,connection,params= args)
            #print(conf_dship_length_df)
    finally:
        connection.close()

conf_dship_length_df['difference'] = (conf_dship_length_df['latest'] - conf_dship_length_df['earliest']).dt.days
        
#print(conf_dship_length_df)

print('Avg months in dship for conf attendees is: ',round(conf_dship_length_df['difference'].mean()/30,2))


# %%


### length in dship for nonconf attendee disciples

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """Select U.id, earliest_discipleship_start_date(U.id) as 'earliest', 
                        case when latest_discipleship_end_date(U.id) IS NULL then curdate() else latest_discipleship_end_date(U.id) END as 'latest' 
                        
                        from users as U 
                        join disciples as D on D.user_id = U.id
                        join campuses as C on C.id = U.campus_id
                        where U.user_role_type_id = 3 
                        and U.school_year_id IN (1,2,3,4,5)
                        and U.id NOT IN %s
                        and C.region_id NOT IN (10,26,44,45)
                        and (latest_discipleship_end_date(U.id) >= %s OR latest_discipleship_end_date(U.id) IS NULL) 
                        and earliest_discipleship_start_date(U.id) <= %s
                        group by U.id
                """
            args = [ids, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            nonconf_dship_length = result
            nonconf_dship_length_df = pd.read_sql(sql,connection,params= args)
            #print(conf_dship_length_df)
    finally:
        connection.close()

nonconf_dship_length_df['difference'] = (nonconf_dship_length_df['latest'] - nonconf_dship_length_df['earliest']).dt.days
        
#print(nonconf_dship_length_df.sort_values('difference', ascending = False))

print('Avg months in dship for nonconf attendees is: ',round(nonconf_dship_length_df['difference'].mean()/30,2))


# %%


#add in and get average to find # of studies! for report
    
    select U.id,U.first_name, U.email, U.last_name,
                        count(distinct case when BSAU.attended = 1 then BSA.id end)
                        from users as U
                        join bible_study_attendances_users as BSAU on BSAU.user_id = U.id
                        join bible_study_attendances as BSA on BSA.id = BSAU.bible_study_attendance_id
                        where BSAU.attended = 1
                        and (earliest_discipleship_start_date(U.id) >= '2019-01-01' or earliest_discipleship_start_date(U.id) IS NULL)
                        and BSA.date between '2018-09-01' and '2019-01-01'
                        and U.id IN
                        group by U.id
                        having count(distinct case when BSAU.attended = 1 then BSA.id end) >= 4


# %%


### conf DMs to SM

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,earliest_discipleship_start_date(U.id),DM.parent_user_id,min(DM.start_date), DS.discipleship_type_id
                        from users as U
                        join disciples as D on D.user_id = U.id
                        join campuses as C on C.id = U.campus_id
                        left join disciples as DM on DM.parent_user_id = U.id
                        left join discipleship_statuses as DS on DS.user_id = DM.parent_user_id 
                        where C.region_id NOT IN(10,26,44,45) 
                        and (DS.end_date IS NULL or DS.end_date >= %s)
                        and DS.start_date <= %s
                        and DS.discipleship_type_id = 5
                        and U.id IN %s
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and min(DM.start_date) <= %s
                """
            args = [spring_end,spring_end,ids, conf_start, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            conf_dms_became_SM = len(result)
            #print(conf_dship_length_df)
    finally:
        connection.close()

       

print('Conf DMs who became SM 3 months post: ',conf_dms_became_SM )


# %%


### conf DMs at start of conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,earliest_discipleship_start_date(U.id),DM.parent_user_id,min(DM.start_date), DS.discipleship_type_id
                        from users as U
                        join disciples as D on D.user_id = U.id
                        join campuses as C on C.id = U.campus_id
                        left join disciples as DM on DM.parent_user_id = U.id
                        left join discipleship_statuses as DS on DS.user_id = DM.parent_user_id 
                        where C.region_id NOT IN(10,26,44,45) 
                        and (DS.end_date IS NULL or DS.end_date >= %s)
                        and DS.start_date <= %s
                        and DS.discipleship_type_id IN (4)
                        and U.id IN %s
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        and min(DM.start_date) <= %s
                """
            args = [conf_start,conf_start,ids, conf_start, conf_start, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            conf_dms_at_start = len(result)
            #print(conf_dship_length_df)
    finally:
        connection.close()

       

print('Conf DMs who were DM at the start: ',conf_dms_at_start )


# %%


### SM who attended Conf

with sshtunnel.SSHTunnelForwarder(
        ('acts247.focus.org', 22),
        ssh_username='bpadmin',
        ssh_pkey=ssh_key_path, 
        remote_bind_address=('127.0.0.1',3306),
        local_bind_address=('127.0.0.1',3306)
) as tunnel:

    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='brian',
                                 password='GWFHAqu99zkpewv8z243b7kSdn6ehR',
                                 db='acts247',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = """select U.id,earliest_discipleship_start_date(U.id),DM.parent_user_id,min(DM.start_date), DS.discipleship_type_id
                        from users as U
                        join disciples as D on D.user_id = U.id
                        join campuses as C on C.id = U.campus_id
                        left join disciples as DM on DM.parent_user_id = U.id
                        left join discipleship_statuses as DS on DS.user_id = DM.parent_user_id 
                        where C.region_id NOT IN(10,26,44,45) 
                        and (DS.end_date IS NULL or DS.end_date >= %s)
                        and DS.start_date <= %s
                        and DS.discipleship_type_id = 5
                        and U.id NOT IN %s
                        group by U.id
                        having earliest_discipleship_start_date(U.id) <= %s
                        and (latest_discipleship_end_date(U.id) IS NULL OR latest_discipleship_end_date(U.id) >= %s)
                        #and min(DM.start_date) <= %s
                """
            args = [spring_end,spring_end,ids, spring_end, spring_end, conf_start]
            cursor.execute(sql, args)
            result = cursor.fetchall()
            current_SM = len(result)
            #print(conf_dship_length_df)
    finally:
        connection.close()

       

print('Current SMs: ',current_SM )


# Repo to be used to store QGI Python code

Jan 2022 edits:
-Summer Projects analysis added to Post Conf script
-Jupytext added for ease of comparison

Jan 2021 edits:
git ignore file added
database edits to connect to Postgres

Needed:
query alterations and accuracy checks

.env file needed to query from database:
ACTS_SSH_USERNAME=
ACTS_SSH_KEY_PATH=
ACTS_SERVER_ADDRESS=
ACTS_MYSQL_USERNAME=
ACTS_DATABASE_NAME=
ACTS_MYSQL_PASSWORD=
FIRST_NAME =
LAST_NAME =
POSTGRES_SSH_USERNAME=
POSTGRES_SSH_KEY_PATH=
POSTGRES_SERVER_ADDRESS=
POSTGRES_USERNAME=
POSTGRES_DATABASE_NAME=
POSTGRES_PASSWORD=
POSTGRES_IP=
EMAIL_USERNAME=
EMAIL_PASSWORD=
SENDGRID_API_KEY=

